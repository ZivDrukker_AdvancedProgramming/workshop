
#include "Source.h"

int main()
{
	while (TRUE)
	{
		vector<string> command = getCommand();

		if (command[0] == "pwd")
		{
			pwd();
		}
		else if (command[0] == "cd")
		{
			cd(command[1]);
		}
		else if (command[0] == "create")
		{
			create(command[1]);
		}
		else if (command[0] == "ls")
		{
			ls();
		}
		else if (command[0] == "secret")
		{
			loadDLL();
		}
		else if (command[0].substr(command[0].length() - 3, 3) == "exe")
		{
			runEXE(command[0]);
		}
	}

	std::system("PAUSE");
	return 0;
}


vector<string> getCommand()
{
	string input;

	cout << "Enter command: ";
	getline(cin, input);
	Helper::trim(input);

	return Helper::get_words(input);
}


void pwd()
{
	LPTSTR buff = new TCHAR[BUFFER];
	DWORD retCode = GetCurrentDirectoryA(BUFFER, buff);

	if (retCode == NULL)
	{
		cout << "Error finding current PATH! ";
		wcout << "The problem might be: " << GetLastError() << endl;
	}
	else
	{
		wcout << "Current path is " << buff << endl;
	}
}


void cd(string path)
{
	LPCTSTR pth = new TCHAR[PATH_MAX];
	pth = path.c_str();
	bool retCode = SetCurrentDirectoryA(pth);

	if (!retCode)
	{
		cout << "Unable to switch directories! ";
		wcout << "The error might be: " << GetLastError() << endl;
	}
	else
	{
		cout << "Done! Path is now " << path << endl;
	}
}


void create(string filename)
{
	LPCTSTR name = new TCHAR[PATH_MAX];
	name = filename.c_str();
	DWORD access = (GENERIC_READ | GENERIC_WRITE);
	DWORD shrMode = (FILE_SHARE_READ | FILE_SHARE_WRITE);
	LPSECURITY_ATTRIBUTES sec = NULL;
	DWORD creation = CREATE_ALWAYS;
	DWORD flags = FILE_ATTRIBUTE_NORMAL;
	HANDLE hndl = NULL;

	HANDLE toDel = CreateFile(name, access, shrMode, sec, creation, flags, hndl);

	if (toDel == INVALID_HANDLE_VALUE)
	{
		wcout << "Error opening file! The problem might be: " << GetLastError() << endl;
	}

	CloseHandle(toDel);
}


void ls()
{
	LPTSTR buff = new TCHAR[BUFFER];
	DWORD retCode = GetCurrentDirectoryA(BUFFER, buff);

	if (retCode == NULL)
	{
		cout << "Error finding current PATH! ";
		wcout << "The problem might be: " << GetLastError() << endl;
	}
	else
	{
		WIN32_FIND_DATA fdd;
		string buffStr = string(buff);
		buffStr += "\\*";

		buff = &buffStr.front();

		HANDLE find = FindFirstFile(buff, &fdd);

		if (find != INVALID_HANDLE_VALUE)
		{
			cout << "The file in the directory are: ";

			do
			{
				wcout << fdd.cFileName << "\t";

			} while (FindNextFile(find, &fdd) != 0);

			cout << endl;
		}
		else
		{
			wcout << "Unable to find files in the directory! The problem might be: " << GetLastError() << endl;
		}
	}
	
}


void loadDLL()
{
	HINSTANCE mod = LoadLibrary("secret.dll");

	if (mod == NULL)
	{
		wcout << "Unable to load dll! Problem might be: " << GetLastError() << endl;
	}
	else
	{
		f_funci func = (f_funci)GetProcAddress(mod, "TheAnswerToLifeTheUniverseAndEverything");
		if (!func) {
			std::cout << "could not locate the function" << std::endl;
		}

		std::cout << "TheAnswerToLifeTheUniverseAndEverything() returned: " << func() << std::endl;
	}
}


void runEXE(string path)
{
	if (WinExec(path.c_str(), NULL) <= 31)
	{
		wcout << "Error! Couldn't run the EXE. The problem might be: " << GetLastError() << endl;
	}
}