#pragma once

#include <iostream>
#include <Windows.h>
#include <vector>
#include "Helper.h"

using namespace std;

typedef int(__stdcall *f_funci)();

#define BUFFER 2048
#define PATH_MAX 2048

vector<string> getCommand();
void pwd();
void cd(string);
void create(string);
void ls();
void loadDLL();
void runEXE(string);